//
//  NetworkCommunicationManager.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import Foundation


protocol NetworkCommunicationManagerDelegate: class {
    
    /* A NetworkCommunicationManager Delegate method to receive the response data from Server.
     @param data - The data received from the web service.
     @param requestIdentifier - for requested Identifier.
     */
    func didReceiveResponseData(response: NSDictionary, requestIdentifier: String)
    
    
    /* A NetworkCommunicationManager Delegate method to receive the error object that indicates why the request failed.
     @param error - Errors returned in the NSURLErrorDomain.
     @param forIdentifier - for requested Identifier.
     */
    func didReceiveError(error: Error, forIdentifier: String)
}

class NetworkCommunicationManager {
    
    
    let baseURL = "http://private-1cc0f-devicecheckout.apiary-mock.com"
    
    weak var ncmDelegate: NetworkCommunicationManagerDelegate?
    
    func requestWithURL(appendString: String, HTTPMethod: String, attributes: NSDictionary, requestIdentifier: String) {
        
        // Server Not Available.
        return;
        
        DispatchQueue.global(qos: .background).async {
            
            let requestURL = URL(string: self.baseURL.appending(appendString))
            let mutableURLRequest: NSMutableURLRequest = NSMutableURLRequest(url: requestURL!)
            mutableURLRequest.httpMethod = HTTPMethod
            mutableURLRequest.httpShouldHandleCookies = false
            mutableURLRequest.timeoutInterval = 90
            mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            
            if attributes.count > 0 {
                // Access thru all the keys, and retrieve corresponding input values from dictionary & set to the NSMutableURLRequest.
                let keyArray:NSArray = attributes.allKeys as NSArray
                for key in keyArray {
                    mutableURLRequest.setValue(attributes.value(forKey: key as! String) as! String?, forHTTPHeaderField: key as! String)
                }
            }
            
            let dataTask = URLSession.shared.dataTask(with: mutableURLRequest as URLRequest) { (data, response, error) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription)
                    self.ncmDelegate?.didReceiveError(error: error!, forIdentifier: requestIdentifier)
                } else {
                    if let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                        self.ncmDelegate?.didReceiveResponseData(response: jsonResult, requestIdentifier: requestIdentifier)
                    }
                }
            }
            // Must resume the task.
            dataTask.resume()
        }
    }
    
    // Function to retreive all devices from Server.
    func getAllDevicesFromServer() {
        self.requestWithURL(appendString:"/devices", HTTPMethod: "GET", attributes: [:], requestIdentifier: "GetAllDevices")
    }
    
    // Function to add a new device to the Server.
    func addNewDeviceToServer(dictionary: NSDictionary) {
        self.requestWithURL(appendString:"/devices", HTTPMethod: "POST", attributes: dictionary, requestIdentifier: "AddNewDevice")
    }
    
    // Function to update the device with given deviceID in Server.
    func updateDeviceInServer(device: Device) {
        
        let mutableDictionary = NSMutableDictionary()
        if device.isCheckedOut {
            mutableDictionary.setValue(device.lastCheckedOutBy, forKey: "lastCheckedOutBy")
            mutableDictionary.setValue(device.lastCheckedOutDate, forKey: "lastCheckedOutDate")
        }
        mutableDictionary.setValue(String(device.isCheckedOut), forKey: "isCheckedOut")
        
        let appString = "/devices/".appending(device.id!)
        self.requestWithURL(appendString:appString, HTTPMethod: "POST", attributes: mutableDictionary, requestIdentifier: "UpdateDevice")
    }
    
    // Function to delete the device with given deviceID in Server.
    func deleteDeviceFromServer(deviceID: String) {
        
        let appString = "/devices/".appending(deviceID)
        self.requestWithURL(appendString:appString, HTTPMethod: "POST", attributes: [:], requestIdentifier: "DeleteDevice")
    }
}

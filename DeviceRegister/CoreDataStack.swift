//
//  CoreDataStack.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import CoreData


class CoreDataStack {
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DeviceRegister")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // Function to save the NSManagedObjectContext.
    func saveContext () {
        
        let managedObjectContext = persistentContainer.viewContext
        
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // Function to insert the data (a new device) into the persistent store.
    func insertDataIntoPersistentStore(attributes: NSDictionary) {
        
        let managedObjectContext = persistentContainer.viewContext
        
        // Create a new managed object.
        let newDevice = NSEntityDescription.insertNewObject(forEntityName: "Device", into: managedObjectContext) as! Device
        
        // Access thru all the keys, and retrieve corresponding input values from dictionary & set to the corresponding attributes of managed object.
        let keyArray:NSArray = attributes.allKeys as NSArray
        
        for key in keyArray {
            if (key as AnyObject).isEqual(to: "isCheckedOut")  {
                let value = attributes.value(forKey: key as! String) as! String?
                newDevice.isCheckedOut = (value != nil)
            } else if (key as AnyObject).isEqual(to: "id")  {
                newDevice.id = attributes.value(forKey: key as! String) as! String?
            } else if (key as AnyObject).isEqual(to: "lastCheckedOutBy")  {
                newDevice.lastCheckedOutBy = attributes.value(forKey: key as! String) as! String?
            } else if (key as AnyObject).isEqual(to: "name" /*"device"*/)  {
                newDevice.name = attributes.value(forKey: key as! String) as! String?
            } else if (key as AnyObject).isEqual(to: "manufacturer")  {
                newDevice.manufacturer = attributes.value(forKey: key as! String) as! String?
            } else if (key as AnyObject).isEqual(to: "os")  {
                newDevice.os = attributes.value(forKey: key as! String) as! String?
            } else if (key as AnyObject).isEqual(to: "lastCheckedOutDate")  {
                let value:String = (attributes.value(forKey: key as! String) as! String?)!
                newDevice.lastCheckedOutDate = Utility.convertStringToDate(string: value) as NSDate?
            }
        }
        
        // Save the NSManagedObjectContext, finally.
        self.saveContext()
    }
    
    // Function to retreive all stored data (devices) from a persistent store.
    func retreiveDataFromPersistentStore() -> NSMutableArray {
        
        let managedObjectContext = persistentContainer.viewContext
        
        // Hold all devices in an array.
        let devices: NSMutableArray = NSMutableArray()
        
        // NSFetchRequest to retrieve data from a persistent store.
        let fetchRequest:NSFetchRequest<Device> = NSFetchRequest<Device>(entityName: "Device")
        let fetchedData = try! managedObjectContext.fetch(fetchRequest)
        if (!fetchedData.isEmpty) {
            for device in fetchedData {
                devices.add(device)
            }
            return devices
        } else {
            return devices
        }
    }
    
    // Function to update the data (device) in a persistent store.
    func updatePersistentStoreWithData(dataToUpdate: Device) {
        
        // Save the NSManagedObjectContext, finally.
        self.saveContext()
    }
    
    // Function to delete the data (device) from a persistent store.
    func deleteDataFromPersistentStore(dataToDelete: Device) {
        
        let managedObjectContext = persistentContainer.viewContext
        
        // Call NSManagedObjectContext delete function to perform deletion of data from a persistent store.
        managedObjectContext.delete(dataToDelete)
        
        // Save the NSManagedObjectContext, finally.
        self.saveContext()
    }
}

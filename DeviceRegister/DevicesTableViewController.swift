//
//  DevicesTableViewController.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import UIKit


class DevicesTableViewController: UITableViewController, NetworkCommunicationManagerDelegate  {
    
    // Collection to hold the devices.
    var deviceList: NSMutableArray!
    
    // Hold the Core Data Stack.
    let cdStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack
    
    // Check if Server data is available or not.
    var dataDownloadedFromServer: Bool!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataDownloadedFromServer = false
        
        // Hold the Network Communication Manager.
        let networkCommManager = (UIApplication.shared.delegate as! AppDelegate).networkCommunicationManager
        networkCommManager.ncmDelegate = self
        networkCommManager.getAllDevicesFromServer()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Server not available, therefore, retreive the device list from persistent store.
        if !dataDownloadedFromServer {
            
            self.deviceList = cdStack.retreiveDataFromPersistentStore() as NSMutableArray!
            
            if self.deviceList.count == 0 {
                
                // Create an UIAlertController, and set it's message.
                let alertController = UIAlertController(title:"Alert!", message:"Device list is empty", preferredStyle: .alert)
                
                // Create Ok UIAlertAction.
                let alertAction:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                alertController.addAction(alertAction)
                
                // Present the alert.
                self.present(alertController, animated: true, completion: nil)
            }
        } else {
            // Check if any data modified in the server or persistent store, if so, synchronize first and reload the device list.
        }
        
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.deviceList != nil {
            return self.deviceList.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellReuseIdentifier", for: indexPath)
        
        // Configure the cell...
        let device = self.deviceList.object(at: indexPath.row) as! Device
        
        cell.textLabel?.text = (device.name as String?)?.appending(" - ".appending((device.os as String?)!))
        if let checkedOut:String =  device.lastCheckedOutBy as String? {
            cell.detailTextLabel?.text = "Last Checked Out By: ".appending(checkedOut)
        } else {
            cell.detailTextLabel?.text = "Last Checked Out By: "
        }
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            // Delete data from persistent store.
            self.cdStack.deleteDataFromPersistentStore(dataToDelete: (self.deviceList.object(at: (indexPath.row)) as! Device))
            
            //let networkCommManager = (UIApplication.shared.delegate as! AppDelegate).networkCommunicationManager
            //networkCommManager.deleteDeviceFromServer(deviceID: (self.deviceList.object(at: (indexPath.row)) as! Device).id!)
            
            // Delete an object at corresponding index.
            self.deviceList.removeObject(at: (indexPath.row))
            
            // Delete the row from the data source.
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Retreive the updated persistent store.
            self.deviceList = cdStack.retreiveDataFromPersistentStore() as NSMutableArray!
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        self.tableView.reloadData()
    }
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "DeviceDetailsVCSegueIdentifier" {
            let viewController:DeviceDetailsViewController = segue.destination as! DeviceDetailsViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            viewController.device = self.deviceList.object(at: (indexPath?.row)!) as! Device
        }
    }
    
    
    // MARK: - NetworkCommunicationManagerDelegate
    
    func didReceiveResponseData(response: NSDictionary, requestIdentifier: String) {
        
        dataDownloadedFromServer = true
        
        if requestIdentifier == "GetAllDevices" {
            // Syncrohonize the local database with data from server.
        }
    }
    
    func didReceiveError(error: Error, forIdentifier: String) {
        
        dataDownloadedFromServer = false
        
        // Create an UIAlertController, and set it's message.
        let alertController = UIAlertController(title:"Error!", message:error.localizedDescription, preferredStyle: .alert)
        
        // Create Ok UIAlertAction.
        let alertAction:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        
        // Present the alert.
        self.present(alertController, animated: true, completion: nil)
    }
}

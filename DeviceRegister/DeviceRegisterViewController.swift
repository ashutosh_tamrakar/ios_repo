//
//  DeviceRegisterViewController.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import UIKit


class DeviceRegisterViewController: UIViewController, NetworkCommunicationManagerDelegate {
    
    @IBOutlet weak var textField_name: UITextField!
    @IBOutlet weak var textField_os: UITextField!
    @IBOutlet weak var textField_manufacturer: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let networkCommManager = (UIApplication.shared.delegate as! AppDelegate).networkCommunicationManager
        networkCommManager.ncmDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - IBAction
    
    // Function to cancel the addition of device.
    @IBAction func cancelButton() {
        
        // If any of the fields have some values entered, alert will pop-up to acknowledge the user of losing entered data.
        if ((!(textField_name.text?.isEmpty)!) || (!(textField_os.text?.isEmpty)!) || (!(textField_manufacturer.text?.isEmpty)!)) {
            
            // Create an UIAlertController, and set it's message.
            let alertController = UIAlertController(title:"Alert!", message:"You are going to lose all entered data. Do you still want to cancel ?", preferredStyle: .alert)
            
            // Create Yes UIAlertAction.
            let yesAlertAction:UIAlertAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesAlertAction)
            
            // Create No UIAlertAction.
            let noAlertAction:UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil)
            alertController.addAction(noAlertAction)
            
            // Present the alert.
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            // Dismiss this view controller.
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Function to save a new device in the application, furthermore, this device will be stored in application's persistent store.
    @IBAction func saveButton() {
        
        // Input to all fields are mandatory, alert will pop-up to acknowledge the user to furnish the empty fields, if any.
        if (((textField_name.text?.isEmpty)!) || ((textField_os.text?.isEmpty)!) || ((textField_manufacturer.text?.isEmpty)!)) {
            
            // Create an UIAlertController, and set it's message.
            let alertController = UIAlertController(title:"Alert!", message:"All fields are required", preferredStyle: .alert)
            
            // Create Ok UIAlertAction.
            let alertAction:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            alertController.addAction(alertAction)
            
            // Present the alert.
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            // Data to store.
            let attributes: NSMutableDictionary = NSMutableDictionary.init()
            attributes.setValue(textField_name.text, forKey: (textField_name.placeholder)!)
            attributes.setValue(textField_os.text, forKey: (textField_os.placeholder)!)
            attributes.setValue(textField_manufacturer.text, forKey: (textField_manufacturer.placeholder)!)
            
            /****** Server not available, following lines are for UI testing purpose only  ******/
            // Get the Core Data Stack.
            let coreDataStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack
            
            attributes.setValue(String(true), forKey: "isCheckedOut")
            attributes.setValue("David", forKey: "lastCheckedOutBy")
            attributes.setValue(Utility.convertDateToString(date: NSDate() as Date), forKey: "lastCheckedOutDate")
            
            var lastDeviceID: String = "1"
            
            if let returnValue: String = UserDefaults.standard.object(forKey: "lastDeviceID") as? String {
                let incrementByOne = 1 + Int(returnValue)!
                lastDeviceID = String(incrementByOne)
            }
            UserDefaults.standard.set(lastDeviceID, forKey: "lastDeviceID")
            UserDefaults.standard.synchronize()
            
            attributes.setValue(lastDeviceID, forKey: "id")
            
            print(attributes)
            
            // Create a new device.
            coreDataStack.insertDataIntoPersistentStore(attributes:attributes)
            
            /***********************************************************************************/
            
            // Add newly created device to server.
            let networkCommManager = (UIApplication.shared.delegate as! AppDelegate).networkCommunicationManager
            networkCommManager.addNewDeviceToServer(dictionary: attributes)
            
            // Dismiss this view controller.
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // MARK: - NetworkCommunicationManagerDelegate
    
    func didReceiveResponseData(response: NSDictionary, requestIdentifier: String) {
        
        if requestIdentifier == "AddNewDevice" {
            
            // Get the Core Data Stack.
            let coreDataStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack
            
            // Create a new device.
            coreDataStack.insertDataIntoPersistentStore(attributes:response)
        }
    }
    
    func didReceiveError(error: Error, forIdentifier: String) {
        
        // Create an UIAlertController, and set it's message.
        let alertController = UIAlertController(title:"Error!", message:error.localizedDescription, preferredStyle: .alert)
        
        // Create Ok UIAlertAction.
        let alertAction:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        
        // Present the alert.
        self.present(alertController, animated: true, completion: nil)
    }
}

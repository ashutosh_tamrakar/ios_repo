//
//  Device+CoreDataProperties.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device");
    }

    @NSManaged public var os: String?
    @NSManaged public var name: String?
    @NSManaged public var manufacturer: String?
    @NSManaged public var lastCheckedOutDate: NSDate?
    @NSManaged public var lastCheckedOutBy: String?
    @NSManaged public var isCheckedOut: Bool
    @NSManaged public var id: String?

}

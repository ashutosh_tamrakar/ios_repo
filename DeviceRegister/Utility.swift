//
//  Utility.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import Foundation

class Utility {
    
    
    // MARK: - Utility Functions
    
    class func convertDateToString(date: Date?) -> (String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from: date!)
        
        return dateString
    }
    
    class func convertStringToDate(string: String) -> (Date?) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: string)
        
        return date!
    }
}

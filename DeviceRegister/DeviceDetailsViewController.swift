//
//  DeviceDetailsViewController.swift
//  DeviceRegister
//
//  Created by ABC on 9/20/16.
//  Copyright © 2016 ABC. All rights reserved.
//

import UIKit

class DeviceDetailsViewController: UIViewController {
    
    public var device: Device! = nil
    
    @IBOutlet weak var textField_name: UITextField!
    @IBOutlet weak var textField_os: UITextField!
    @IBOutlet weak var textField_manufacturer: UITextField!
    @IBOutlet weak var textField_lastCheckedOut: UITextField!
    @IBOutlet weak var textField_checkedOutTime: UITextField!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Details"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title:"CheckIn", style: .plain, target: self, action:#selector(DeviceDetailsViewController.actionToRegister))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.textField_name?.text = device.name
        self.textField_os?.text = device.os
        self.textField_manufacturer?.text = device.manufacturer
        
        if device.isCheckedOut {
            self.navigationItem.rightBarButtonItem?.title = "Check In"
            self.textField_lastCheckedOut?.text = device.lastCheckedOutBy
            if device.lastCheckedOutDate != nil {
                self.textField_checkedOutTime?.text = Utility.convertDateToString(date: device.lastCheckedOutDate! as Date)
            }
        } else {
            self.navigationItem.rightBarButtonItem?.title = "Check Out"
            self.textField_lastCheckedOut?.text = "N/A"
            if device.lastCheckedOutDate != nil {
                self.textField_checkedOutTime?.text = "N/A"
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UIBarButtonItem
    
    func actionToRegister() {
        
        let text:String = (self.navigationItem.rightBarButtonItem!.title)!
        if  text == "Check In" {
            device.lastCheckedOutBy = "N/A"
            device.lastCheckedOutDate = NSDate()
            device.isCheckedOut = false
            
        } else if text == "Check Out" {
            self.textField_lastCheckedOut?.text = "N/A"
            if let checkedOutTime: String = self.textField_checkedOutTime?.text {
                if checkedOutTime == "N/A" {
                    device.lastCheckedOutDate = NSDate()
                    device.lastCheckedOutBy = "Michael"
                }
                device.isCheckedOut = true
            }
        }
        
        //let coreDataStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack
        //coreDataStack.updatePersistentStoreWithData(dataToUpdate: device)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - NetworkCommunicationManagerDelegate
    
    func didReceiveResponseData(response: NSDictionary, requestIdentifier: String) {
        
        if requestIdentifier == "UpdateDevice" {
            
            let keyArray:NSArray = response.allKeys as NSArray
            for key in keyArray {
                if (key as AnyObject).isEqual(to: "isCheckedOut")  {
                    let value = response.value(forKey: key as! String) as! String?
                    device.isCheckedOut = (value != nil)
                } else if (key as AnyObject).isEqual(to: "id")  {
                    device.id = response.value(forKey: key as! String) as! String?
                } else if (key as AnyObject).isEqual(to: "lastCheckedOutBy")  {
                    device.lastCheckedOutBy = response.value(forKey: key as! String) as! String?
                } else if (key as AnyObject).isEqual(to: "name" /*"device"*/)  {
                    device.name = response.value(forKey: key as! String) as! String?
                } else if (key as AnyObject).isEqual(to: "manufacturer")  {
                    device.manufacturer = response.value(forKey: key as! String) as! String?
                } else if (key as AnyObject).isEqual(to: "os")  {
                    device.os = response.value(forKey: key as! String) as! String?
                } else if (key as AnyObject).isEqual(to: "lastCheckedOutDate")  {
                    let value:String = (response.value(forKey: key as! String) as! String?)!
                    device.lastCheckedOutDate = Utility.convertStringToDate(string: value) as NSDate?
                }
            }
            
            // Get the Core Data Stack.
            let coreDataStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack
            
            // Create a new device.
            coreDataStack.updatePersistentStoreWithData(dataToUpdate: device)
        }
    }
    
    func didReceiveError(error: Error, forIdentifier: String) {
        
        // Create an UIAlertController, and set it's message.
        let alertController = UIAlertController(title:"Error!", message:error.localizedDescription, preferredStyle: .alert)
        
        // Create Ok UIAlertAction.
        let alertAction:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        
        // Present the alert.
        self.present(alertController, animated: true, completion: nil)
    }
}

# DeviceRegister: Developed in Swift 3.0.

This application provides a dashboard to view device list with technical details and current status of availability. It also features to add a new devices to the list.
The application is implemented to work over internet connection, exchange data by GET & POST request with server. Core Data is used to support offline.


## Application Architecture


Core Data Stack: The application uses CoreData for offline support, performing various opertions such as storing - insertion of new device, retrieval - fetching devices from the store, updation and deletion of devices. Leveraging NSPersistentContainer, a new class introduce in iOS 10.0 SDK simplifies and reduces the effort on creating a Core Data Stack, loads the store, maintain references of all required objects to function Core Data Stack.

Network Communication Manager: Handles all data exchange to/from the server in a background thread, for instance, getting device list, adding a new device to server, deleting device from server and updating the changes of corresponding device. The request/response are in JSON Data Format. Perform HTTP GET & POST method requests to communicate with the server. It's protocol has 2 functions to call back and update the delegates upon receiving the response back from the server.

ViewControllers: Manages the display of the corresponding views, populating data from Server and from persistent store, business logics are written here. Co-ordinator between different modules such as CoreDataStack & NetworkCommunicationManager. Delegates of NetworkCommunicationManager to receive the data and act accordingly to show the data on views.



## Requirements

### Build

Xcode 8.0 or later, iOS 10.0 SDK or later, Swift 3.0

### Runtime

iOS 10.0.

Copyright (C) 2016 Apple Inc. All rights reserved.

